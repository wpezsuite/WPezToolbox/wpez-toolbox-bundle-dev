<?php
/**
 * Plugin Name: WPezToolbox - Bundle: Dev
 * Plugin URI: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev
 * Description: DEV - A WPezToolbox bundle of developer-centic WordPress plugin tools.
 * Version: 0.0.3
 * Author: Mark F. Simchock (Chief Executive Alchemist) at Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez_tbox
 *
 * @package WPezToolboxBundleDev
 */

namespace WPezToolboxBundleDev;

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__ . '\plugins' );

/**
 * Callback for the filter: WPezToolboxLoader\plugins.
 *
 * @param array $arr_in The array of plugins already hooked to the filter.
 *
 * @return array
 */
function plugins( array $arr_in = array() ) {

	$str_bundle = __( 'DEV', 'wpez_tbox' );
	// TODO - Get this from the main plugin.
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = array(
		'debug-this'                                => array(
			'name'     => $str_prefix . 'Debug This',
			'slug'     => 'debug-this',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'MachoThemes',
					'url_by'  => 'https://www.machothemes.com/',
					'desc'    => 'For admins, developers, and support staff, Debug This provides a ton of information about your WordPress installation, all from the front-end admin bar.',
					'url_img' => 'https://ps.w.org/debug-this/assets/icon-256x256.png?rev=1555481',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/debug-this/',
				),
			),
		),

		'debug-bar'                                 => array(
			'name'     => $str_prefix . 'Debug Bar',
			'slug'     => 'debug-bar',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'wordpressdotorg',
					'desc'    => 'Adds a debug menu to the admin bar that shows query, cache, and other helpful debugging information.',
					'url_img' => 'https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/debug-bar/',
				),
			),
		),

		'wpez-toolbox-bundle-debug-bar-add-ons'     => array(
			'name'     => $str_prefix . 'Bundle: Debug Bar Add-Ons',
			'slug'     => 'wpez-toolbox-bundle-debug-bar-add-ons',
			'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-debug-bar-add-ons/-/archive/master/wpez-toolbox-bundle-debug-bar-add-ons-master.zip',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Mark F. Simchock (Chief Executive Alchemist) for Alchemy United',
					'url_by'  => 'https://AlchemyUnited.com',
					'desc'    => 'DBB+ -  A WPezToolbox bundle of third-party add-ons for the famous Debug Bar plugin.',
					'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15292865/wpeztoolbox.png?width=64',
				),
				'resources' => array(
					'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-debug-bar-add-ons',
					'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
					'url_tw'   => 'https://twitter.com/WPezDeveloper',
				),
			),
		),

		'duplicator'                                => array(
			'name'     => $str_prefix . 'Duplicator',
			'slug'     => 'duplicator',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Snap Creek',
					'url_by'  => 'https://snapcreek.com/',
					'desc'    => 'Duplicator successfully gives WordPress users the ability to migrate, copy, move or clone a site from one location to another and also serves as a simple backup utility.',
					'url_img' => 'https://ps.w.org/duplicator/assets/icon-128x128.png?rev=2083921',
				),
				'resources' => array(
					'url_wp_org'  => 'https://wordpress.org/plugins/duplicator/',
					'url_site'    => 'https://snapcreek.com/duplicator/',
					'url_premium' => 'https://snapcreek.com/duplicator/pricing/',
					'url_tw'      => 'https://twitter.com/duplicatorpro',
					'url_docs'    => 'https://snapcreek.com/docs/',
				),
			),
		),

		'query-monitor'                             => array(
			'name'     => $str_prefix . 'Query Monitor',
			'slug'     => 'query-monitor',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'John Blackbourn',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/johnbillion/',
					'desc'    => 'Query Monitor enables debugging of database queries, PHP errors, hooks and actions, block editor blocks, enqueued scripts and stylesheets, HTTP API calls, and more.',
					'url_img' => 'https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/query-monitor/',
					'url_repo'   => 'https://github.com/johnbillion/query-monitor',
					'url_site'   => 'https://querymonitor.com/',
					'url_tw'     => 'https://twitter.com/johnbillion',
				),
			),
		),

		'wpez-toolbox-bundle-query-monitor-add-ons' => array(
			'name'     => $str_prefix . 'Bundle: Query Monitor Add-Ons',
			'slug'     => 'wpez-toolbox-bundle-query-monitor-add-ons',
			'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-query-monitor-add-ons/-/archive/master/wpez-toolbox-bundle-query-monitor-add-ons-master.zip',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Mark F. Simchock (Chief Executive Alchemist) for Alchemy United',
					'url_by'  => 'https://AlchemyUnited.com',
					'desc'    => 'QM+ - A WPezToolbox bundle of third-party add-ons for the ever-popular Query Monitor plugin.',
					'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15292865/wpeztoolbox.png?width=64',
				),
				'resources' => array(
					'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-query-monitor-add-ons',
					'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
					'url_tw'   => 'https://twitter.com/WPezDeveloper',
				),
			),
		),

		'restricted-site-access'                    => array(
			'name'     => $str_prefix . 'Restricted Site Access',
			'slug'     => 'restricted-site-access',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Jake Goldman, 10up, Oomph',
					'by_alt'  => 'Jake\'s WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/jakemgold/',
					'desc'    => 'Limit access your site to visitors who are logged in or accessing the site from a set of specified IP addresses. Send restricted visitors to the log in page, redirect them, or display a message or page. A great solution for Extranets, publicly hosted Intranets, or parallel development / staging sites. Adds a number of new configuration options to the Reading settings panel as well as the Network Settings panel in multisite. ',
					'url_img' => 'https://ps.w.org/restricted-site-access/assets/icon-128x128.png?rev=2138472',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/restricted-site-access/',
					'url_repo'   => 'https://github.com/10up/restricted-site-access',
					'url_site'   => 'https://10up.com/plugins/restricted-site-access-wordpress/',
					'url_fb'     => 'https://www.facebook.com/10up.agency',
					'url_tw'     => 'https://twitter.com/10up',
				),
			),
		),

		'debug-objects'                             => array(
			'name'     => $str_prefix . 'Debug Objects',
			'slug'     => 'debug-objects',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Frank Bültge',
					'url_by'  => 'https://bueltge.de/',
					'desc'    => 'Debug Objects provides the user, which has the appropriate rights, normally the administrator, a large number of information: query, cache, cron, constants, hooks, functions and many many more.',
					'url_img' => 'https://s.w.org/plugins/geopattern-icon/debug-objects_d0d0d1.svg',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/debug-objects/',
				),
			),
		),

		'wp-rollback'                               => array(
			'name'     => $str_prefix . 'WP Rollback',
			'slug'     => 'wp-rollback',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Impress.org',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/impressorg/',
					'desc'    => 'Quickly and easily rollback any theme or plugin from WordPress.org to any previous (or newer) version without any of the manual fuss. Works just like the plugin updater, except you’re rolling back (or forward) to a specific version. No need for manually downloading and FTPing the files or learning Subversion. This plugin takes care of the trouble for you.',
					'url_img' => 'https://ps.w.org/wp-rollback/assets/icon-128x128.jpg?rev=1159170',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/wp-rollback/',
					'url_repo'   => 'https://github.com/impress-org/wp-rollback/',
					'url_site'   => 'https://impress.org/',
					'url_tw'     => 'https://twitter.com/ImpressOrg',
					'url_docs'   => 'https://snapcreek.com/docs/',
				),
			),
		),

		'updraftplus'                               => array(
			'name'     => $str_prefix . 'UpdraftPlus Backup/Restore (Free)',
			'slug'     => 'updraftplus',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'UpdraftPlus.Com, DavidAnderson',
					'url_by'  => 'https://updraftplus.com/',
					'desc'    => 'UpdraftPlus simplifies backups and restoration. It is the world’s highest ranking and most popular scheduled backup plugin, with over two million currently-active installs. Backup your files and database backups into the cloud and restore with a single click!',
					'url_img' => 'https://ps.w.org/updraftplus/assets/icon-128x128.jpg?rev=1686200',
				),
				'resources' => array(
					'url_wp_org'  => 'https://wordpress.org/plugins/updraftplus/',
					'url_site'    => 'https://updraftplus.com/wordpress-backup-plugin-comparison/',
					'url_premium' => 'https://updraftplus.com/shop/updraftplus-premium/',
					'url_docs'    => 'https://updraftplus.com/support/',
				),
			),
		),

		'user-switching'                            => array(
			'name'     => $str_prefix . 'User Switching',
			'slug'     => 'user-switching',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'John Blackbourn & contributors',
					'by_alt'  => 'WP.org Profile',
					'url_by'  => 'https://profiles.wordpress.org/johnbillion/',
					'desc'    => 'Quickly swap between user accounts in WordPress at the click of a button. You’ll be instantly logged out and logged in as your desired user.',
					'url_img' => 'https://ps.w.org/user-switching/assets/icon.svg?rev=2032062',
				),
				'resources' => array(
					'url_wp_org' => 'https://wordpress.org/plugins/user-switching/',
					'url_repo'   => 'https://github.com/johnbillion/user-switching',
					'url_tw'     => 'https://twitter.com/johnbillion',
				),
			),
		),

		'wpez-toolbox-bundle-dev-themes'            => array(
			'name'     => $str_prefix . 'Bundle: Dev Themes',
			'slug'     => 'wpez-toolbox-bundle-dev-themes',
			'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev-themes/-/archive/master/wpez-toolbox-bundle-dev-themes-master.zip',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Mark F. Simchock (Chief Executive Alchemist) for Alchemy United',
					'url_by'  => 'https://AlchemyUnited.com',
					'desc'    => 'DEVT - A WPezToolbox bundle of developer-centic WordPress plugin tools for working on themes.',
					'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15502083/wpeztoolbox.png?width=64',
				),
				'resources' => array(
					'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev-themes',
					'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
					'url_tw'   => 'https://twitter.com/WPezDeveloper',
				),
			),
		),

		'spatie-ray'                                => array(
			'name'     => $str_prefix . 'Spatie Ray',
			'slug'     => 'spatie-ray',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Spatie',
					'url_by'  => 'https://spatie.be',
					'desc'    => 'Ray is a beautiful, lightweight desktop app that helps you debug your PHP-based application.',
					'url_img' => 'https://ps.w.org/spatie-ray/assets/icon.svg?rev=2455693',
				),
				'resources' => array(
					'url_wp_org'  => 'https://wordpress.org/plugins/spatie-ray',
					'url_repo'    => 'https://github.com/spatie/ray',
					'url_site'    => 'https://myray.app/',
					'url_tw'      => 'https://twitter.com/spatie_be',
					// TODO - Check main plugin, YouTube supported?
					'url_yt'      => 'https://www.youtube.com/channel/UCoBbei3S9JLTcS2VeEOWDeQ',
					'url_premium' => 'https://spatie.be/products/ray',
					'url_docs'    => 'https://spatie.be/docs/ray/v1/introduction',
				),
			),
		),

		'wp-reroute-email'                          => array(
			'name'     => $str_prefix . 'WP Reroute Email',
			'slug'     => 'wp-reroute-email',
			'required' => false,
			'wpez'     => array(
				'info'      => array(
					'by'      => 'Sajjad Hossain',
					'url_by'  => 'https://sajjadhossain.com/',
					'desc'    => 'This plugin intercepts all outgoing emails from a WordPress site, sent using the wp_mail() function, and reroutes them to a predefined configurable email address.',
					'url_img' => 'https://s.w.org/plugins/geopattern-icon/wp-reroute-email_d1e8de.svg',
				),
				'resources' => array(
					'url_wp_org'  => 'https://wordpress.org/plugins/wp-reroute-email/',
				),
			),
		),

	);

	$arr_mod = apply_filters( __NAMESPACE__ . '/plugins', $arr );

	// Use $arr if the filter doesn't return an array.
	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}
