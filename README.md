## WPezToolbox - Bundle: Dev 

__A WPezToolbox bundle of developer-centic WordPress plugin tools.__



> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Important

The plugins contained in WPezToolbox bundles are simply "you might consider these suggestions." They are not formal endorsements; there is not claim being made about their quality and/or functionality. Also, some links might be affiliate links. 

### Overview

See the README here: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### This Bundle Includes

- https://wordpress.org/plugins/debug-this

- https://wordpress.org/plugins/debug-bar

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-debug-bar-add-ons

- https://wordpress.org/plugins/duplicator

- https://wordpress.org/plugins/query-monitor

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-query-monitor-add-ons

- https://wordpress.org/plugins/debug-objects

- https://wordpress.org/plugins/restricted-site-access/

- https://wordpress.org/plugins/wp-rollback

- https://wordpress.org/plugins/updraftplus

- https://wordpress.org/plugins/user-switching

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-dev-themes



- 



### Helpful Links

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### TODO 



### CHANGE LOG

- v0.0.1 - 25 January 2020
   - ADDED: Restricted Site Access

- v0.0.0 - 18 November 2019
   - Proof of Concept

